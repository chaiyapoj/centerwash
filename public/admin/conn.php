<?php
date_default_timezone_set("Asia/Bangkok");
function connect()
{
    $host = "127.0.0.1";
    $username = "root";
    $password = "";
    $dbname = "centerwash";
    $charset = "utf8";

    // Create connection
    global $conn;
    $conn = new mysqli($host, $username, $password, $dbname);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    mysqli_set_charset($conn, $charset);

    return $conn;
}

function select($table)
{
    global $conn;
    $sql = "$table";
    $res = $conn->query($sql) or die("SQL Error: <br>" . $sql . "<br>" . $conn->error);
    $result = $res->fetch_assoc();
    return $result;
}

function setabel($condition)
{
    global $conn;
    $sql = "$condition";
    $res = $conn->query($sql) or die("SQL Error: <br>" . $sql . "<br>" . $conn->error);
    $result = $res->fetch_assoc();
    return $result;
}

function selects($condition)
{
    global $conn;
    $sql = "$condition";
    $res = $conn->query($sql) or die("SQL Error: <br>" . $sql . "<br>" . $conn->error);

    return $res;
}

function insert($table)
{
    global $conn;
    $sql = "$table";
    if ($conn->query($sql)) {
        return true;
    } else {
        die("SQL Error: <br>" . $sql . "<br>" . $conn->error);
        return false;
    }
}


function num_row($table)
{
    global $conn;
    $sql = "select count(*) AS numroww from $table ";
    $res = $conn->query($sql) or die("SQL Error: <br>" . $sql . "<br>" . $conn->error);
    $result = $res->fetch_assoc();
    return $result;
}

function update($where)
{
    global $conn;

    $sql = ("$where");
    if ($conn->query($sql)) {
        return true;
    } else {
        die("SQL Error: <br>" . $sql . "<br>" . $conn->error);
        return false;
    }
}


function delete($where)
{
    global $conn;
    $sql = "$where";
    if ($conn->query($sql)) {
        return true;
    } else {
        die("SQL Error: <br>" . $sql . "<br>" . $conn->error);
        return false;
    }
}

function DateThai($strDate)
{
    $strYear = date("Y", strtotime($strDate)) + 543;
    $strMonth = date("n", strtotime($strDate));
    $strDay = date("j", strtotime($strDate));
    $strHour = date("H", strtotime($strDate));
    $strMinute = date("i", strtotime($strDate));
    $strSeconds = date("s", strtotime($strDate));
    $strMonthCut = Array("", "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");
    $strMonthThai = $strMonthCut[$strMonth];
    return "$strDay $strMonthThai $strYear, $strHour:$strMinute";
}

function DateThaiFull($strDate)
{
    $strYear = date("Y", strtotime($strDate)) + 543;
    $strMonth = date("n", strtotime($strDate));
    $strDay = date("j", strtotime($strDate));
    $strHour = date("H", strtotime($strDate));
    $strMinute = date("i", strtotime($strDate));
    $strSeconds = date("s", strtotime($strDate));
    $strMonthCut = Array("", "มกราคม", "กุมภาพันธ์", "มีนาคม.", "เมษายน", "พฤษภาคม", "มิถุนายน.", "กรกฏาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤษศจิกายน", "ธันวาคม");
    $strMonthThai = $strMonthCut[$strMonth];
    return "$strDay $strMonthThai $strYear";
}

function typenotify($data)
{
    switch ($data) {
        case ("1"or"2"or"3"or"4"or"5"):
            echo "<i class='fa  fa-database'></i>";
            break;
    }
}

function textnotify($data)
{
    switch ($data) {
        case "1":
            echo "รอการอนุมัติ";
            break;

        case "2":
            echo "รับการพิจารณา";
            break;

        case "3":
            echo "ผ่านการพิจารณา";
            break;

        case "4":
            echo "ไม่ผ่านการพิจารณา";
            break;

        case "5":
            echo "ระงับการเผยแผร่";
            break;
    }
}


function colornotify($data)
{
    switch ($data) {
        case "1":
            echo "bg-warning";
            break;

        case "2":
            echo "bg-info";
            break;

        case "3":
            echo "bg-success";
            break;

        case "4":
            echo "bg-danger";
            break;

        case "5":
            echo "bg-danger";
            break;


        default:
            echo "bg-secondary";
    }

}

function fb_date($timestamp)
{
    $difference = time() - $timestamp;
    $periods = array("second", "minute", "hour");
    $ending = " ago";
    if ($difference < 60) {
        $j = 0;
        $periods[$j] .= ($difference != 1) ? "s" : "";
        $difference = ($difference == 3 || $difference == 4) ? "a few " : $difference;
        $text = "$difference $periods[$j] $ending";
    } elseif ($difference < 3600) {
        $j = 1;
        $difference = round($difference / 60);
        $periods[$j] .= ($difference != 1) ? "s" : "";
        $difference = ($difference == 3 || $difference == 4) ? "a few " : $difference;
        $text = "$difference $periods[$j] $ending";
    } elseif ($difference < 86400) {
        $j = 2;
        $difference = round($difference / 3600);
        $periods[$j] .= ($difference != 1) ? "s" : "";
        $difference = ($difference != 1) ? $difference : "about an ";
        $text = "$difference $periods[$j] $ending";
    } elseif ($difference < 172800) {
        $difference = round($difference / 86400);
        $periods[$j] .= ($difference != 1) ? "s" : "";
        $text = "Yesterday at " . date("g:ia", $timestamp);
    } else {
        if ($timestamp < strtotime(date("Y-01-01 00:00:00"))) {
            $text = date("l j, Y", $timestamp) . " at " . date("g:ia", $timestamp);
        } else {
            $text = date("l j", $timestamp) . " at " . date("g:ia", $timestamp);
        }
    }
    return $text;
}

function notifynumrow() {
    if($_SESSION['level']=="1" OR $_SESSION['level']=="4"){
        $sqlnotify=sprintf(" SELECT count(*) AS numroww FROM `notification` WHERE status =1 and user_level= 2 ");
        $data=select($sqlnotify);
        $sqlnotify2=sprintf(" SELECT count(*) AS numroww FROM `notification` WHERE status =1 AND user_id2= %s ",$_SESSION['id']);
        $data2=select($sqlnotify2);

        echo $data["numroww"]+$data2["numroww"];
    }else{
        $sqlnotify=sprintf(" SELECT count(*) AS numroww FROM `notification` WHERE status =1 AND user_id2= %s ",$_SESSION['id']);
        $data=select($sqlnotify);
        echo $data["numroww"];
    }

}

function checkid($data){
    if(!empty($data)){ echo "&idsend=".$data.""; }
}

?>