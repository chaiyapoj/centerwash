<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Mitr" rel="stylesheet">
    <style type="text/css">
        body {
            font-family: 'Mitr', sans-serif;
        }
    </style>
</head>
<body>

<div class="container">
    <!--    <h2>Card Header and Footer</h2>-->
    <br>
    <?php
    if (!empty($_GET['e']) AND $_GET['e']) {
        ?>
        <div class="alert bg-danger alert-dismissible text-white">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>แจ้งเตือน!</strong> ชื่อผู้ใช้งาน รหัสผ่านไม่ถูกต้อง
        </div>

        <?php
    }

    ?>
    <div class="card ">
        <div class="card-header bg-warning text-white text-uppercase ">Smart Center Wash System</div>
        <div class="card-body">
            <form method="post" action="chklogin.php">
                <div class="form-group">
                    <label for="email">ชื่อผู้ใช้งาน :</label>
                    <input type="text" name="username" class="form-control input-lg" required>
                </div>
                <div class="form-group">
                    <label for="pwd">รหัสผ่าน :</label>
                    <input type="password" name="password" class="form-control input-sm" required>
                </div>

                <!--                <div class="form-check">-->
                <!--                    <label class="form-check-label">-->
                <!--                        <input class="form-check-input" type="checkbox"> Remember me-->
                <!--                    </label>-->
                <!--                </div>-->
                <button type="submit" class="btn btn-warning btn-lg btn-block">เข้าสู่ระบบ</button>
            </form>
        </div>
        <div class="card-footer text-center">พัฒนาระบบโดย วิทยาลัยเทคนิคสุราษฎร์ธานี</div>
    </div>
</div>

</body>
</html>
