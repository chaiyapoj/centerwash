<?php
session_start();
if ( !isset($_SESSION['id']) ){
    echo "<meta http-equiv='refresh' content='0;URL=index.php'>";
}
//echo $_SESSION['id'];

include("conn.php");
connect();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Mitr" rel="stylesheet">
    <style type="text/css">
        body {
            font-family: 'Mitr', sans-serif;
        }
    </style>
</head>
<body>

<div class="container">
    <!--    <h2>Card Header and Footer</h2>-->
    <br>
    <?php
    if (!empty($_GET['e']) AND $_GET['e']=="updatesuccess") {
        ?>
        <div class="alert bg-success alert-dismissible text-white">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>แจ้งเตือน!</strong> ระบบเริ่มทำการนับยอดใหม่ สำเร็จ
        </div>

        <?php
    }
    $sql = sprintf("SELECT * FROM `jobs` LEFT join customers ON jobs.customer_id=customers.id join washers on jobs.washer_id=washers.id ");
    $res = selects($sql);
    $total = 0;
    $total2 = 0;
    $total3 = 0;
    while ($data = $res->fetch_assoc()) {
        //echo "1";
        //$total = $total + $data['balance'];
        $total = $total + $data['price'];
        //$total2 = $total2 + $data['balance'];
       // $total3 = $total3 + $data['price'];
    }

    $sql22 = sprintf("SELECT * FROM `jobs` LEFT join customers ON jobs.customer_id=customers.id join washers on jobs.washer_id=washers.id WHERE `checked` IS NULL ");
    $res22 = selects($sql22);
    while ($data = $res22->fetch_assoc()) {
        //echo "1";
        //$total = $total + $data['balance'];
        //$total = $total + $data['price'];
        //$total2 = $total2 + $data['balance'];
        $total3 = $total3 + $data['price'];
    }


    ?>
    <div class="card ">
        <div class="card-header bg-warning text-white text-uppercase "><i class="fa fa-tachometer fa-2x"
                                                                          aria-hidden="true"></i> หน้าหลัก


            <a href="exit.php" ><button class="btn btn-danger pull-right"><i class="fa fa-sign-out" aria-hidden="true"></i> ออกจากระบบ</button></a>
            <a href=#" ><button class="btn  bg-warning pull-right text-uppercase">    </button></a>
            <a href="exit.php" ><button class="btn btn-primary pull-right text-uppercase"><i class="fa fa-repeat" aria-hidden="true"></i> Refash</button></a>
            <a href=#" ><button class="btn  bg-warning pull-right text-uppercase">    </button></a>
            <a href="sql.php"  onClick="javascript:return confirm('ต้องการเริ่มต้นการนับยอดใหม่  หรือไม่?');" ><button class="btn btn-success pull-right text-uppercase"><i class="fa fa-history" aria-hidden="true"></i> เริ่มรอบใหม่</button></a>

        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="card ">
                        <div class="card-header bg-success text-white text-uppercase ">รายได้ทั้งหมด</div>
                        <div class="card-body">
                            <h4><i class="fa fa-btc "></i> <?php echo number_format(" " .$total."",2)."<br>"; ?></h4>
                        </div>
                    </div>

                </div>

                <div class="col-sm-6">
                    <div class="card ">
                        <div class="card-header bg-primary text-white text-uppercase ">รายได้จากการใช้บริการ</div>
                        <div class="card-body">
                            <h4><i class="fa fa-btc "></i> <?php echo number_format(" " .$total3."",2)."<br>"; ?></h4>
                        </div>
                    </div>

                </div>
            </div>
            <hr>
            <div class="row ">
                <div class="col-12 table-responsive">
                    <ul class="nav nav-pills" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="pill" href="#home">รายได้ทั้งหมด</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="pill" href="#menu1">รายการเดินบัญชี</a>
                        </li>
                        <!--                        <li class="nav-item">-->
                        <!--                            <a class="nav-link" data-toggle="pill" href="#menu2">Menu 2</a>-->
                        <!--                        </li>-->
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div id="home" class="container tab-pane active"><br>
                            <h3>รายได้ทั้งหมด</h3>
                            <hr>
                            <table class="table table-hover " width="100%">
                                <thead>
                                <tr class="bg-success">
                                    <th width="40%">วันที่</th>
                                    <th width="30%">เบอร์โทร</th>

                                    <th width="30%">ยอดเงินทำรายการ</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $sql1 = sprintf("SELECT jobs.created_at,jobs.checked,customers.phoneNumber,washers.price FROM `jobs` LEFT join customers ON jobs.customer_id=customers.id join washers on jobs.washer_id=washers.id ORDER BY created_at DESC");
                                $res1 = selects($sql1);

                                while ($data1 = $res1->fetch_assoc()) {
                                    ?>
                                    <tr>
                                        <td><?=$data1['created_at']?></td>
                                        <td><?=$data1['phoneNumber']?></td>
                                        <td>

                                            <?php
                                            echo number_format(" " .$data1['price']."",2)."<br>";

                                            ?>
                                        </td>

                                    </tr>
                                <?php

                                }
                                ?>


                                </tbody>
                            </table>
                        </div>
                        <div id="menu1" class="container tab-pane fade"><br>
                            <h3>รายการเดินบัญชี</h3>
                            <hr>
                            <?php
                            $sql1 = sprintf("SELECT jobs.created_at,jobs.checked,customers.phoneNumber,washers.price FROM `jobs` LEFT join customers ON jobs.customer_id=customers.id join washers on jobs.washer_id=washers.id WHERE `checked` IS NULL ORDER BY created_at DESC");
                            $res1 = selects($sql1);
                            if(!$res1){
                                echo "ไม่มีรายการบัญชี";
                                }else{
                                ?>
                            <?php
                            }
                                    ?>
                            <table class="table table-hover " width="100%">
                                <thead>
                                <tr class="bg-primary">
                                    <th width="40%">วันที่</th>
                                    <th width="30%">เบอร์โทร</th>

                                    <th width="30%">ยอดเงินทำรายการ</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php



                                while ($data1 = $res1->fetch_assoc()) {
                                    ?>
                                    <tr>
                                        <td><?=$data1['created_at']?></td>
                                        <td><?=$data1['phoneNumber']?></td>
                                        <td>

                                            <?php
                                            echo number_format(" " .$data1['price']."",2)."<br>";

                                            ?>
                                        </td>

                                    </tr>
                                    <?php

                                }
                                ?>


                                </tbody>
                            </table>


                        </div>
                        <!--                        <div id="menu2" class="container tab-pane fade"><br>-->
                        <!--                            <h3>Menu 2</h3>-->
                        <!--                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>-->
                        <!--                        </div>-->
                    </div>

                </div>
            </div>

        </div>
        <div class="card-footer text-center">พัฒนาระบบโดย วิทยาลัยเทคนิคสุราษฎร์ธานี</div>
    </div>
</div>

</body>
</html>
