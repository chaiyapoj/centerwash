<?php

use Illuminate\Database\Seeder;

class WashersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('washers')->insert([
            'name' => 'No.1',
            'price' => '20',
            'ip_address' => '192.168.0.101',
            'status' => '0'
        ]);

        DB::table('washers')->insert([
            'name' => 'No.2',
            'price' => '30',
            'ip_address' => '192.168.0.102',
            'status' => '0'
        ]);

        DB::table('washers')->insert([
            'name' => 'No.3',
            'price' => '40',
            'ip_address' => '192.168.0.103',
            'status' => '0'
        ]);
    }
}
