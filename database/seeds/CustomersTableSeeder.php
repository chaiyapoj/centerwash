<?php

use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert([
            'phoneNumber' => '1111111111',
            'balance' => 10,
            'password' => 1234
        ]);

        DB::table('customers')->insert([
            'phoneNumber' => '2222222222',
            'balance' => 0,
            'password' => 1234
        ]);
    }
}