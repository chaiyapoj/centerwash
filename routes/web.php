<?php

use GuzzleHttp\Client;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/burm', function () {
    return view('burm');
});

Route::get('/', 'ServiceController@index')->name('index');


Route::POST('/inputphonenumber', 'ServiceController@inputPhoneNumber');
Route::post('/customerpassword', 'ServiceController@CustomerPassword');
Route::post('/setcustomerpassword', 'ServiceController@setCustomerPassword');


Route::post('/checkpassword', 'ServiceController@checkPassword');

Route::get('/choosePrice/{price}', 'ServiceController@ChoosePrice');

Route::post('/sendwasherrequest', 'ServiceController@SendWasherRequest');
Route::post('/savebalance', 'ServiceController@saveBalance');

Route::post('/sendwasherrequestqueue', 'ServiceController@SendWasherRequestQueue');


Route::get('/line1', 'ServiceController@linetest');

Route::get('/backend', 'BackendController@SumJob');


