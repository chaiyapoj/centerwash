@extends('layout.master')
@section('content')

    <body background="img/4.jpg">
    <div class="container">
        <br><br><br><br><br><br>
        <div class="row">
            <div class="col-sm-4">
                <a href="/choosePrice/20" class="btn btn-warning">20 บาท <br> <h4 class="text-white">( ขนาด 7 กก.)</h4>
                </a></a>
            </div>

            <div class="col-sm-4">
                <a href="/choosePrice/30" class="btn btn-warning">30 บาท<br> <h4 class="text-white">( ขนาด 12 กก. )</h4>
                </a></a>
            </div>

            <div class="col-sm-4">
                <a href="#">
                    <a href="/choosePrice/40" class="btn btn-warning">40 บาท <br> <h4 class="text-white">( ขนาด 15 กก. )</h4></a>
                </a>
            </div>
        </div>
    </div>
    @stop

    @push('css')
        <style>
            .btn {
                padding: 0px 25px;
                font-size: 63px;
                border-radius: 10px;
            }
        </style>
    @endpush
