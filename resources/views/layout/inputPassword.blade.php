@extends('layout.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <br><br><br><br><br>
                <div class="text-center">
                    <form name="myForm" action="#" method="post" onsubmit="return validateForm()">
                        <div class="form-group">
                            <input type="password" name="pass" id="show" maxlength="6" style="height:70px;font-size: 50px;" class="form-control form-control-lg" placeholder="รหัสผ่าน..." readonly>
                        </div>
                        <br><br><br><br><br><br>
                        <input type="button" class="btn btn-danger" value="ยกเลิก" style="padding: 5px 25px;font-size: 40px;" onclick="inputnum(this.value)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="submit" class="btn btn-success" value="ยืนยัน" style="padding: 5px 25px;font-size: 40px;">
                    </form>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="text-center">
                    <br>
                    <div class="row">
                        <div class="col-sm-12">
                            <input type="button" class="btn btn-warning" value="1" onclick="inputnum(this.value)">&nbsp;&nbsp;&nbsp;
                            <input type="button" class="btn btn-warning" value="2" onclick="inputnum(this.value)">&nbsp;&nbsp;&nbsp;
                            <input type="button" class="btn btn-warning" value="3" onclick="inputnum(this.value)">
                        </div>
                    </div>
                    <p>
                    <div class="row">
                        <div class="col-sm-12">
                            <input type="button" class="btn btn-warning" value="4" onclick="inputnum(this.value)">&nbsp;&nbsp;&nbsp;
                            <input type="button" class="btn btn-warning" value="5" onclick="inputnum(this.value)">&nbsp;&nbsp;&nbsp;
                            <input type="button" class="btn btn-warning" value="6" onclick="inputnum(this.value)">
                        </div>
                    </div>
                    <p>
                    <div class="row">
                        <div class="col-sm-12">
                            <input type="button" class="btn btn-warning" value="7" onclick="inputnum(this.value)">&nbsp;&nbsp;&nbsp;
                            <input type="button" class="btn btn-warning" value="8" onclick="inputnum(this.value)">&nbsp;&nbsp;&nbsp;
                            <input type="button" class="btn btn-warning" value="9" onclick="inputnum(this.value)">
                        </div>
                    </div>
                    <p>
                    <div class="row">
                        <div class="col-sm-12">
                            <input type="button" class="btn btn-warning" value="0" onclick="inputnum(this.value)">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@push('javascript')
    <script type="text/javascript">
        function inputnum(a) {
            if (a == 'ยกเลิก') {
                document.getElementById("show").value = '';
            } else {
                document.getElementById("show").value += a;
            }
        }

        function validateForm() {
            var x = document.forms["myForm"]["pass"].value;
            if (x == "") {
                return false;
            }
        }
    </script>
@endpush