@extends('layout.master')
@section('content')
    <body background="/img/5.jpg">
    <div class="container" id="app">
        <br><br><br><br><br>

        <form action="/sendwasherrequestqueue" method="POST">
            {{ csrf_field() }}
            <div class="row">

                <div class="col-sm-2">
                </div>

                <div class="col-sm-3">
                    <input v-model="currentBalance" type="text" name="balance"
                           style="height:70px;font-size: 50px;text-align: center;"
                           class="form-control form-control-lg" readonly>
                </div>
                <br><br><br><br>

                <div class="col-sm-7">
                </div>

                <div class="col-sm-2">
                </div>

                <div class="col-sm-3">
                    <input v-model="sessionPrice" type="text" name="2"
                           style="height:70px;font-size: 50px;text-align: center;"
                           class="form-control form-control-lg" readonly>
                </div>
                <br><br><br><br>

                <div class="col-sm-7">
                </div>

                <div class="col-sm-2">
                </div>

                <div class="col-sm-3">
                    <input v-model="currentMore" type="text" name="3"
                           style="height:70px;font-size: 50px;text-align: center;"
                           class="form-control form-control-lg" readonly>
                </div>

                <div class="col-sm-1">
                </div>
                <form class="col-sm-6">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                    <input type="submit" class="btn btn-warning" value="ยืนยัน"
                           :disabled="disableButton == 1 ? true : false">
                </form>

                <form action="/savebalance" method="POST">{{ csrf_field() }}<input type="hidden" v-model="saveBalance"
                                                                                   name="balance"><input type="submit"
                                                                                                         value="ยกเลิก"
                                                                                                         class="btn btn-danger">
                </form>&nbsp;&nbsp;
            </div>
    </div>

    </div>
    </div>
    </body>
@stop
@push('javascript')
    <script src="//{{ Request::getHost() }}:6001/socket.io/socket.io.js"></script>
    <script src="{{ mix('/js/app.js') }}"></script>
    <script>
        const app = new Vue({
            el: '#app',
            data() {
                return {
                    disableButton: 1,
                    insertedCoin: 0,
                }
            },
            methods: {
                init() {

                    if (this.currentMore < 1) {
                        this.disableButton = 0;
                    }


                    Echo.channel('washer')
                        .listen('CoinAccepted', (e) => {
                            this.insertedCoin += parseInt(e.msg);
                            if (this.currentMore < 1) {
                                this.disableButton = 0;
                            }
                        });
                }
            },
            computed: {
                saveBalance: function () {
                    return {{ Session::get('balance') }} +this.insertedCoin;
                },
                sessionBalance: function () {
                    return {{ Session::get('balance') }};
                },
                sessionPrice: function () {
                    return {{ Session::get('price') }};
                },
                currentBalance: function () {
                    let ch = this.sessionBalance - this.sessionPrice + this.insertedCoin;
                    if (ch < 0) {
                        return 0;
                    } else {
                        return ch;
                    }
                },
                currentMore: function () {
                    let left = this.sessionPrice - this.sessionBalance - this.insertedCoin;
                    if (left < 0) {
                        return 0;
                    } else {
                        return left;
                    }
                },
                insertedCoin: function () {
                    Echo.channel('washer')
                        .listen('CoinAccepted', (e) => {
                            let more = this.currentMore;
                            if (more < 0) {
                                this.disableButton = 0;
                                this.currentBalance += parseInt(e.msg);
                                return left;
                            } else {
                                this.insertedCoin += parseInt(e.msg);
                                return this.insertedCoin;
                            }

                        });
                }
            },
            created() {
                this.init();
                // ถ้ามีเงิน
                if (this.sessionBalance > 0) {
                    if (this.sessionBalance = this.session) {
                        this.insertedCoin += this.balance;
                        this.balance = 0
                        this.disableButton = 0;

                    }
                }

            }
        })

    </script>
@endpush
@push('css')
    <style>
        .btn {
            padding: 0px 25px;
            font-size: 35px;
            border-radius: 10px;
        }
    </style>
@endpush