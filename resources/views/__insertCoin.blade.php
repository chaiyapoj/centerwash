@extends('layout.master')
@section('content')
    <body background="/img/5.jpg">
    <div class="container" id="app">
        <br><br><br><br><br>

        <form action="/sendwasherrequest" method="POST">
            {{ csrf_field() }}
            <div class="row">

                <div class="col-sm-2">
                </div>

                <div class="col-sm-3">
                    <input v-model="currentBalance" type="text" name="1"
                           style="height:70px;font-size: 50px;text-align: center;"
                           class="form-control form-control-lg" readonly>
                </div>
                <br><br><br><br>

                <div class="col-sm-7">
                </div>

                <div class="col-sm-2">
                </div>

                <div class="col-sm-3">
                    <input v-model="sessionPrice" type="text" name="2"
                           style="height:70px;font-size: 50px;text-align: center;"
                           class="form-control form-control-lg" readonly>
                </div>
                <br><br><br><br>

                <div class="col-sm-7">
                </div>

                <div class="col-sm-2">
                </div>

                <div class="col-sm-3">
                    <input v-model="currentMore" type="text" name="3"
                           style="height:70px;font-size: 50px;text-align: center;"
                           class="form-control form-control-lg" readonly>
                </div>

                <div class="col-sm-1">
                </div>
                <div class="col-sm-6">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="http://localhost" class="btn btn-danger">ยกเลิก</a>&nbsp;&nbsp;
                    <input type="submit" class="btn btn-warning" value="ยืนยัน"
                           :disabled="disableButton == 1 ? true : false">>&nbsp;&nbsp;
                </div>
            </div>
        </form>
    </div>
    </div>
    </body>
@stop
@push('javascript')
    <script src="//{{ Request::getHost() }}:6001/socket.io/socket.io.js"></script>
    <script src="{{ mix('/js/app.js') }}"></script>
    <script>
        const app = new Vue({
            el: '#app',
            data() {
                return {
                    disableButton: 1,
                    coin: 0
                }
            },
            methods: {
                init: function () {
                    Echo.channel('washer')
                        .listen('CoinAccepted', (e) => {
                            console.log('Inserted : ' + e.msg);
                            this.coin += parseInt(e.msg);
                        });
                }
            },
            computed: {
                sessionBalance: function () {
                    return {{ Session::get('balance') }};
                },
                sessionPrice: function () {
                    return {{ Session::get('price') }};
                },
                currentBalance: function () {
                    return 0;
                },
                currentMore: function () {
                    return 0;
                }
            },
            mounted() {
                this.init();
            }
        })
    </script>
@endpush
@push('css')
    <style>
        .btn {
            padding: 0px 25px;
            font-size: 35px;
            border-radius: 10px;
        }
    </style>
@endpush