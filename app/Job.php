<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class job extends Model
{
    public function customer()
    {
        return $this->belongsTo('App\customer');

    }

    public function washer()
    {
        return $this->belongsTo('App\washer');

    }
}
