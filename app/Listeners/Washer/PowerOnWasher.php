<?php

    namespace App\Listeners\Washer;

    use App\Events\SendWasherRequest;
    use Illuminate\Queue\InteractsWithQueue;
    use Illuminate\Contracts\Queue\ShouldQueue;
    use GuzzleHttp\Client;

    class PowerOnWasher
    {
        /**
         * Create the event listener.
         *
         * @return void
         */
        public function __construct()
        {
            //
        }

        /**
         * Handle the event.
         *
         * @param  SendWasherRequest $event
         * @return void
         */
        public function handle(SendWasherRequest $event)
        {
            $client = new Client();
            $client->get('https://banditdee.info/line/json.php?phoneNumber=' . $event->job->customer->phoneNumber . '&status=2');
            $client->get('http://' . $event->job->washer->ip_address . '/?2');
        }
    }
