<?php

namespace App\Listeners\Line;

use App\Events\ReceiveWasherRequest;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PushLineMessage
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ReceiveWasherRequest  $event
     * @return void
     */
    public function handle(ReceiveWasherRequest $event)
    {
        //
    }
}
