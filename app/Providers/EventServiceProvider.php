<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\SendWasherRequest' => [
            'App\Listeners\Washer\PowerOnWasher'
        ],
        'App\Events\ReceiveWasherRequest' => [
            'App\Listeners\Line\PushLineMessage',
            'App\Listeners\Washer\CheckNextJob'
        ],
        'App\Events\CoinAccepted' => [],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
