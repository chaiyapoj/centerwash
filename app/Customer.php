<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class customer extends Model
{

    protected $guarded = [
        'id'
    ];

    public function hasQueueList()
    {

        return (bool)$this->QueueList()->first();
    }

    public function QueueList()
    {
        return $this->hasOne('App\QueueList');

    }

    public function job()
    {
        return $this->hasOne('App\job');

    }

}
