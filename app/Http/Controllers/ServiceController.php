<?php

namespace App\Http\Controllers;

use App\Events\ReceiveWasherRequest;
use App\Events\SendWasherRequest;
use Illuminate\Http\Request;
use App\Customer;
use App\Washer;
use GuzzleHttp\Client;
use Alert;
use App\QueueList;
use Illuminate\Support\Facades\Session;
use App\job;

class ServiceController extends Controller
{

    public function index()
    {
        Session::forget('customer_id');
        Session::forget('price');
        Session::forget('balance');
        Session::forget('washer_id');
        return view('inputPhoneNumber');
    }


    public function inputPhoneNumber(Request $request)
    {
        $phoneNumber = $request->get('phoneNumber');
        session(['phonenumber' => $phoneNumber]);
        $customer = customer::firstOrCreate(['phoneNumber' => $phoneNumber]);
        session(['customer_id' => $customer->id]);
        session(['balance' => $customer->balance]);


        if ($customer->wasRecentlyCreated) {
            Alert::warning('กรุณาตั้งรหัสผ่าน')->autoclose(2000);

            return view('setPassword');
        } else {
            Alert::warning('กรุณากรอกรหัสผ่าน')->autoclose(2000);
            return view('inputPassword');
        }
    }


    public function setCustomerPassword(Request $request)
    {
        $customer_id = session('customer_id');
        session(['balance' => 0]);

        $customer = customer::find($customer_id);
        $customer->password = $request->get('password');
        $customer->save();

        Alert::warning('กรุณาเลือกราคา')->autoclose(3000);
        return view('choosePrice');

    }

    public function checkPassword(Request $request)
    {
        $password = $request->get('password');
        $customer_id = session('customer_id');
        $customer = customer::find($customer_id);

        if ($customer->password !== $password) {
            Alert::warning('รหัสผ่านไม่ถูกต้อง')->autoclose(2000);
            return view('inputPassword');
        }

        if ($customer->hasQueueList()) {

            if ($customer->QueueList->status == 1) {
                Alert::warning('Warning Message', 'ถึงคิวของ');
                $client = new Client();
                $client->get('https://banditdee.info/line/json.php?phoneNumber=' . $customer->phoneNumber . '&status=2');
                $client->get('http://192.168.0.101/2');
            } else {
                Alert::warning('ยังไท่ถึงคิวของท่าน กรุณารอการแจ้งเตือนจาก Line')->autoclose(5000);
                return view('inputPhoneNumber');
            }
        }
        Alert::warning('เลือกราคา')->autoclose(3000);
        return view('choosePrice');
    }

    public function ChoosePrice($price)
    {
        session(['price' => $price]);

        $washer = Washer::where('price', $price)->where('status', 0)
            ->first();

/*        $client = new Client();
        $client->get('http://192.168.0.83:8080/coinopen');
        $client->get('http://192.168.0.83:8080/bankopen');*/

        if (!$washer) {
            Alert::warning('Warning Message', 'ต้องจอง');

            return view('insertCoinQueue');
        } else {

            session(['washer_id' => $washer->id]);
            return view('insertCoin');
        }

    }


    public function SendWasherRequest(Request $request)
    {
        $client = new Client();
        $client->get('http://192.168.0.83:8080/coinclose');
        $client->get('http://192.168.0.83:8080/bankclose');

        $balance = $request->get('balance');
        $customer_id = session('customer_id');

        $customer = Customer::find($customer_id);
        $customer->balance = $balance;
        $customer->save();

        $washer_id = session('washer_id');
        $customer_id = session('customer_id');


        $job = new job;
        $job->washer_id = $washer_id;
        $job->customer_id = $customer_id;
        $job->save();

        $washer = Washer::find($washer_id);
        $washer->status = 1;
        $washer->save();

        $client = new Client();
        $client->get('http://192.168.0.83:8080/coinclose');
        $client->get('http://192.168.0.83:8080/bankclose');

        $client->get('https://banditdee.info/line/json.php?phoneNumber=' . $job->customer->phoneNumber . '&status=2');
        $client->get('http://' . $washer->ip_address . '/2');

        return redirect()->route('index');

    }

    public function saveBalance(Request $request)
    {

        $client = new Client();

        $client->get('http://192.168.0.83:8080/coinclose');
        $client->get('http://192.168.0.83:8080/bankclose');

        $customer_id = session('customer_id');
        $balance = $request->get('balance');

        $customer = Customer::find($customer_id);
        $customer->balance = $balance;
        $customer->save();


        return redirect()->route('index');

    }

    public function SendWasherRequestQueue()
    {

        $client = new Client();
        $client->get('http://192.168.0.83:8080/coinclose');
        $client->get('http://192.168.0.83:8080/bankclose');

        $price = session('price');
        $customer_id = session('customer_id');

        $QueueList = new QueueList;
        $QueueList->price = $price;
        $QueueList->customer_id = $customer_id;
        $QueueList->save();

        $customer = Customer::find($customer_id);
        $client = new Client();
        $client->get('https://banditdee.info/line/json.php?phoneNumber=' . $customer->phoneNumber . '&status=4');

        Alert::warning('จองคิวเรียบร้อย กรุณารอการแจ้งเตือนเมื่อถึงคิว')->autoclose(3000);
        return redirect()->route('index');

    }


    public function ReceiveWasherRequest(Request $request)
    {
        $ip_address = $request->ip();
        $washer = washer::where('ip_address', $ip_address)->first();
        $washer->status = 0;
        $washer->save();

        $job = job::where('washer_id', $washer->id)->latest()->first();
        $job->done = 1;
        $job->save();

        $customer_id = $job->customer_id;

        $customer = Customer::find($customer_id);
        $client = new Client();
        $client->get('https://banditdee.info/line/json.php?phoneNumber=' . $customer->phoneNumber . '&status=3');

        $QueueList = QueueList::where('price', $washer->price)->latest()->first();
        if ($QueueList !== null) {
            $QueueList->status = 1;
            $QueueList->save();

            $client->get('https://banditdee.info/line/json.php?phoneNumber=' . $QueueList->customer->phoneNumber . '&status=1');
        }
    }


}

