<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Job;

class BackendController extends Controller
{
    public function SumJob()
    {
        $jobs = Job::all();
        $sum = $jobs->washer->sum('price');
        return view('backend.sumjob')->with(compact($sum));
    }
}
