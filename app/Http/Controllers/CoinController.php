<?php

    namespace App\Http\Controllers;

    use Illuminate\Http\Request;
    use App\Events\CoinAccepted;

    class CoinController extends Controller
    {
        public function CoinAccepted($msg)
        {
            event(new CoinAccepted($msg));
        }
    }
