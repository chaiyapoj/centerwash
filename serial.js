const SerialPort = require('serialport');
var http = require('http');
var coin, coin2;
var url = "http://192.168.0.100/api/coin/";
const mySerial = new SerialPort('COM9', {
    baudRate: 9600
});

const mySerial2 = new SerialPort('COM10', {
    baudRate: 9600
});

mySerial.on('data', function (data) {
    coin = data.toString('hex');
    //console.log(coin);
    if(coin == 7906 || coin == 06) {
        console.log("20 Bath");
        http.get(url+"20");
    }else if(coin == 7907 || coin == 07){
        console.log("50 Bath");
        http.get(url+"50");
    }else if(coin == 7908 || coin == 08){
        console.log("100 Bath");
        http.get(url+"100");
    }
});

mySerial2.on('data', function (data) {
    coin2 = data.toString('hex');
    if(coin2 == "9006120103ac" || coin2 == "06120103ac" || coin2 == "120103ac" || coin2 == "0103ac" || coin2 == "03ac" || coin2 == "ac" ||
        coin2 == "9006120603b1" || coin2 == "06120603b1" || coin2 == "120603b1" || coin2 == "0603b1" || coin2 == "03b1" || coin2 == "b1") {
        console.log("1 Bath");
        http.get(url+"1");
    }else if(coin2 == "9006120203ad" || coin2 == "06120203ad" || coin2 == "120203ad" || coin2 == "0203ad" || coin2 == "03ad" || coin2 == "ad" ||
        coin2 == "9006120503b0" || coin2 == "06120503b0" || coin2 == "120503b0" || coin2 == "0503b0" || coin2 == "03b0" || coin2 == "b0"){
        console.log("2 Bath");
        http.get(url+"2");
    }else if(coin2 == "9006120303ae" || coin2 == "06120303ae" || coin2 == "120303ae"|| coin2 == "0303ae"|| coin2 == "03ae"|| coin2 == "ae"){
        console.log("5 Bath");
        http.get(url+"5");
    }else if(coin2 == "9006120403af" || coin2 == "06120403af" || coin2 == "120403af" || coin2 == "0403af" || coin2 == "03af" || coin2 == "af"){
        console.log("10 Bath");
        http.get(url+"10");
    }
});